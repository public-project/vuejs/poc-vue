import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/login/Login'
import Home from '@/components/pages/Home'
import Shipmaster from '@/components/pages/Shipmaster'
import Masterwork from '@/components/pages/Masterwork'
import User from '@/components/pages/User'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/hello',
      name: 'Hello',
      component: HelloWorld
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/shipmaster',
      name: 'Shipmaster',
      component: Shipmaster
    },
    {
      path: '/masterwork',
      name: 'Masterwork',
      component: Masterwork
    },
    {
      path: '/user',
      name: 'User',
      component: User
    }
  ]
})
